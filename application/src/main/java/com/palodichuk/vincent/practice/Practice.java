/*
 * File: Practice.java
 */
package com.palodichuk.vincent.practice;

import com.palodichuk.vincent.practice.string.DecimalToBinary;

import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * An application that uses the various modules to provide for some
 * interactive fun!
 */
public class Practice {
    /**
     * The entry point for the application.
     *
     * @param args command line arguments; currently ignored.
     */
    public static void main(String[] args) {
        try (final Scanner scanner = new Scanner(System.in)) {
            System.out.print("Enter a base 10 integer or Q to quit: ");
            while (scanner.hasNext()) {
                String s = scanner.next();
                scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

                if (s.equalsIgnoreCase("q"))
                    break;

                try {
                    long n = Long.parseLong(s);

                    DecimalToBinary decimalToBinary = new DecimalToBinary(n);

                    System.out.println("Bit string: " + decimalToBinary.getBitString());
                    System.out.println("Maximum number of ones in a row: " + decimalToBinary.getMaxOnesInARow());
                    System.out.println();
                } catch (NumberFormatException ex) {
                    System.out.println("That number is too big, too small, or not a number.");
                    System.out.println("The minimum number is " + Long.MIN_VALUE + " and the maximum number is " + Long.MAX_VALUE + ".");
                }
                System.out.print("Enter a base 10 integer or Q to quit: ");
            }
        } catch (IllegalStateException | NoSuchElementException ex) {
            System.out.println(ex);
        }
    }
}
