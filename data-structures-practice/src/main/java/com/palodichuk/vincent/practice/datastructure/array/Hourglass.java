/*
 * File: Hourglass.java
 */
package com.palodichuk.vincent.practice.datastructure.array;

/**
 * A class that performs an "Hourglass" sum on an array of integers.
 *
 * Given a 6x6 2D Array, A:
 *
 * 1 1 1 0 0 0
 * 0 1 0 0 0 0
 * 1 1 1 0 0 0
 * 0 0 0 0 0 0
 * 0 0 0 0 0 0
 * 0 0 0 0 0 0
 *
 * We define an hourglass in A to be a subset of values with indices falling in
 * this pattern in A's graphical representation:
 *
 * a b c
 *   d
 * e f g
 *
 * There are 16 hourglasses in A, and an hourglass sum is the sum of an hourglass' values.
 */
public class Hourglass {
    private int[][] array;

    /**
     * Constructs a new instance based on the specified array.
     *
     * @param array the integer array to find the maximum hourglass sum from
     * @throws NullPointerException indicates that the specified array is null
     * @throws IllegalArgumentException indicates that the specified array is
     * smaller than a 3 x 3 2D array.
     */
    public Hourglass(int[][] array) {
        if (array == null) {
            throw new NullPointerException("The specified array is null.");
        }

        if (array.length < 3 || array[0].length < 3) {
            throw new IllegalArgumentException("The specified array is less than a 3 x 3 array.");
        }

        this.array = array;
    }

    /**
     * Performs an "Hourglass" sum of the array and returns the hourglass with the
     * maximum sum.
     *
     * @return the maximum hourglass sum.
     */
    public final int sum() {
        int maxSum = Integer.MIN_VALUE;

        for (int i = 0, j = 0; i < array.length - 2 && j < array[i].length - 2; i++) {
            while (j < array[i].length - 2) {
                int sum = sumRow(array[i], j, true)
                        + sumRow(array[i + 1], j, false) + sumRow(array[i + 2], j, true);

                if (sum > maxSum) {
                    maxSum = sum;
                }

                j++;
            }
            j = 0;
        }

        return maxSum;
    }

    /**
     * Helper method to sum an hourglass row.
     *
     * @param arr the array row being summed
     * @param j the starting index in the row
     * @param allThree if true, all three elements are summed,
     *                 else just the middle element.
     * @return the requested sum.
     */
    private static final int sumRow(int[] arr, int j, boolean allThree) {
        if (allThree) {
            return arr[j] + arr[j + 1] + arr[j + 2];
        } else {
            return arr[j + 1];
        }
    }
}
