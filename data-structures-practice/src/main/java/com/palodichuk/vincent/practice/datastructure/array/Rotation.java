/*
 * File: Rotation.java
 */
package com.palodichuk.vincent.practice.datastructure.array;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * A rotation operation on an array of size n shifts each of the array's elements 1
 * unit in the specified direction. For example, if 2 left rotations are performed
 * on array [1, 2, 3, 4, 5], then the array would become [3, 4, 5, 1, 2]. This
 * class supports rotating the array either left or right any number of times.
 *
 * @param <T> The type of data the array holds
 */
public class Rotation<T> {
    private T[] array;

    /**
     * Instantiates a new instance based on the specified array.
     *
     * @param array the array that we will be operating on. Must contain at least two elements.
     * @throws NullPointerException indicates that the specified array is null
     * @throws IllegalArgumentException indicates that the specified array contains
     * fewer than 2 elements.
     */
    public Rotation(T[] array) {
        if (array == null) {
            throw new NullPointerException("The specified array is null.");
        }

        if (array.length < 2) {
            throw new IllegalArgumentException("The specified array contains less than two elements.");
        }

        this.array = array;
    }

    /**
     * Rotates the array to the left the specified number of times.
     *
     * @param numTimes the number of times to rotate left. Must be greater than 0.
     */
    public void left(int numTimes) {
        if (numTimes < 1) {
            throw new IllegalArgumentException("The specified number of rotations must be positive.");
        }

        if (Math.floorMod(array.length - numTimes, array.length) == 0)
            return; // No need to rotate as we have come full circle!

        Map<Integer, T> temps = new HashMap<>();

        for (int i = array.length - 1; i >= 0; i--) {
            int dest = Math.floorMod(i + (array.length - numTimes), array.length);
            T source = null;

            // Save our destination entry for retrieval later
            temps.put(dest, array[dest]);

            if (temps.containsKey(i)) {
                source = temps.get(i);
                temps.remove(i);
            } else {
                source = array[i];
            }

            array[dest] = source;
        }
    }

    /**
     * Rotates the array to the right the specified number of times.
     *
     * @param numTimes the number of times to rotate right. Must be greater than 0.
     */
    public void right(int numTimes) {
        if (numTimes < 1) {
            throw new IllegalArgumentException("The specified number of rotations must be positive.");
        }

        if (Math.floorMod(array.length + numTimes, array.length) == 0)
            return; // No need to rotate as we have come full circle!

        Map<Integer, T> temps = new HashMap<>();

        for (int i = 0; i < array.length; i++) {
            int dest = Math.floorMod(i + (array.length + numTimes), array.length);
            T source = null;

            // Save our destination entry for retrieval later
            temps.put(dest, array[dest]);

            if (temps.containsKey(i)) {
                source = temps.get(i);
                temps.remove(i);
            } else {
                source = array[i];
            }

            array[dest] = source;
        }
    }

    /**
     * Gets the array associated with this rotation object
     *
     * @return the array associated with this rotation object
     */
    public T[] getArray() {
        return array;
    }

    /**
     * A toString method that prints the array as space separated values if spaces is set to true.
     *
     * @param spaces if true, the array is printed as space separated characters.
     *
     * @return the array as a string.
     */
    public String toString(boolean spaces) {
        if (!spaces)
            return toString();

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < array.length; i++) {
            sb.append(array[i]);

            if (i + 1 < array.length)
                sb.append(" ");
        }

        return sb.toString();
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Rotation{");
        sb.append("array=").append(array == null ? "null" : Arrays.asList(array).toString());
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rotation<?> rotation = (Rotation<?>) o;
        return Arrays.equals(getArray(), rotation.getArray());
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(getArray());
    }
}
