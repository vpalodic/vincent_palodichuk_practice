/*
 * File: MinimumSwaps.java
 */
package com.palodichuk.vincent.practice.datastructure.array;

/**
 * You are given an unordered array consisting of consecutive
 * integers E [1, 2, 3, ..., n] without any duplicates.
 * You are allowed to swap any two elements. You need to find
 * the minimum number of swaps required to sort the array in ascending order.
 *
 * For example, given the array arr = [7, 1, 3, 2, 4, 5, 6]
 * we perform the following steps:
 *
 * i   arr                     swap (indices)<br>
 * 0   [7, 1, 3, 2, 4, 5, 6]   swap (0,3)<br>
 * 1   [2, 1, 3, 7, 4, 5, 6]   swap (0,1)<br>
 * 2   [1, 2, 3, 7, 4, 5, 6]   swap (3,4)<br>
 * 3   [1, 2, 3, 4, 7, 5, 6]   swap (4,5)<br>
 * 4   [1, 2, 3, 4, 5, 7, 6]   swap (5,6)<br>
 * 5   [1, 2, 3, 4, 5, 6, 7]<br><br>
 * It took 5 swaps to sort the array.
 */
public class MinimumSwaps {
    private int[] array;

    /**
     * Instantiates a new instance with the specified array.
     *
     * @param array the array to sort and count the minimum number of swaps
     * @throws NullPointerException indicates that the specified array is null
     * @throws IllegalArgumentException indicates that the specified array contains
     * fewer than 2 elements.
     */
    public MinimumSwaps(int[] array) {
        if (array == null) {
            throw new NullPointerException("The specified array is null.");
        }

        if (array.length < 2) {
            throw new IllegalArgumentException("The specified array contains less than two elements.");
        }

        this.array = array;
    }

    /**
     * Performs a modified insertion sort on the associated array and returns
     * the minimum number of swaps to perform the sort.
     *
     * @return the minimum number of swaps to perform the sort.
     */
    public int sort() {
        int swaps = 0;

        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] != i + 1) {
                for (int j = i + 1; j < array.length; j++) {
                    if (array[j] != j + 1 && array[j] == i + 1) {
                        swaps++;
                        int t = array[i];
                        array[i] = array[j];
                        array[j] = t;
                        break;
                    }
                }
            }
        }

        return swaps;
    }
}
