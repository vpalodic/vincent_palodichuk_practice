package com.palodichuk.vincent.practice.datastructure.array;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;

import static org.junit.Assert.*;

@RunWith(JUnitParamsRunner.class)
public class HourglassTest {
    final int NUM_ROWS = 6;
    final int NUM_COLUMNS = 6;
    final int RANGE = 19;
    final int SUB = 9;

    private static Object[] getNullArray(){
        return new Object[] {
                null
        };
    }

    private static Object[] getTooSmallIntegerArray() {
        return new Object[] {
                new Object[] { new int[][] { {1, 2}, {3, 4}, {5, 6}}},
                new Object[] { new int[][] { {1, 2, 3}, {4, 5, 6}}}
        };
    }

    private static Object[] getCustomValidSeedsAndMax() {
        return new Object[] {
                new Object[] {11072018, 16},
                new Object[] {11082018, 30},
                new Object[] {11092018, 19}
        };
    }

    @Test(expected = NullPointerException.class)
    @Parameters(method = "getNullArray")
    public void testConstructorNull(int[][] array) {
        Hourglass hourglass = new Hourglass(array);
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters(method = "getTooSmallIntegerArray")
    public void testConstructorIllegal(int[][] array) {
        Hourglass hourglass = new Hourglass(array);
    }

    @Test
    @Parameters(method = "getCustomValidSeedsAndMax")
    public void testHourglassSum(int seed, int maxSum) {
        final int SEED = seed;
        final Random random = new Random(SEED);

        int[][] arr = new int[NUM_ROWS][NUM_COLUMNS];

        for (int i = 0; i < NUM_ROWS; i++) {
            for (int j = 0; j < NUM_COLUMNS; j++) {
                arr[i][j] = (random.nextInt(RANGE) % RANGE) - SUB;
            }
        }

        for (int i = 0; i < NUM_ROWS; i++) {
            for (int j = 0; j < NUM_COLUMNS; j++) {
                System.out.print(String.format("%3d", arr[i][j]));

                if (j + 1 < NUM_COLUMNS)
                    System.out.print(", ");
                else {
                    System.out.println();
                }
            }

            System.out.println();
        }

        Hourglass hourglass = new Hourglass(arr);
        int sum = hourglass.sum();

        assertEquals(maxSum, sum);
        System.out.println("Max Hourglass Sum: " + sum);
    }

}