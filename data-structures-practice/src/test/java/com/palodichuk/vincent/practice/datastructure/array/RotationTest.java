package com.palodichuk.vincent.practice.datastructure.array;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class RotationTest {

    private Object[] getNullArray() {
        return new Object[] {(Integer[])null};
    }

    private Object[] getTooSmallIntegerArray() {
        return new Object[] {
                new Object[] {new Integer[] {}},
                new Object[] {new Integer[] {1}}
        };
    }

    private Object[] getIllegalRotations() {
        return new Object[] {
                new Object[] {new Integer[] {1, 2, 3, 4, 5}, 0, ""},
                new Object[] {new Integer[] {1, 2, 3, 4, 5}, -1, ""},
        };
    }

    private Object[] getCustomLeftRotationIntegerArray() {
        return new Object[] {
                new Object[] {new Integer[] {1, 2, 3, 4, 5}, 1, "2 3 4 5 1"},
                new Object[] {new Integer[] {1, 2, 3, 4, 5}, 2, "3 4 5 1 2"},
                new Object[] {new Integer[] {1, 2, 3, 4, 5}, 3, "4 5 1 2 3"},
                new Object[] {new Integer[] {1, 2, 3, 4, 5}, 4, "5 1 2 3 4"},
                new Object[] {new Integer[] {1, 2, 3, 4, 5}, 5, "1 2 3 4 5"},
                new Object[] {new Integer[] {1, 2, 3, 4, 5}, 6, "2 3 4 5 1"},
        };
    }

    private Object[] getCustomRightRotationIntegerArray() {
        return new Object[] {
                new Object[] {new Integer[] {1, 2, 3, 4, 5}, 1, "5 1 2 3 4"},
                new Object[] {new Integer[] {1, 2, 3, 4, 5}, 2, "4 5 1 2 3"},
                new Object[] {new Integer[] {1, 2, 3, 4, 5}, 3, "3 4 5 1 2"},
                new Object[] {new Integer[] {1, 2, 3, 4, 5}, 4, "2 3 4 5 1"},
                new Object[] {new Integer[] {1, 2, 3, 4, 5}, 5, "1 2 3 4 5"},
                new Object[] {new Integer[] {1, 2, 3, 4, 5}, 6, "5 1 2 3 4"},
        };
    }

    @Test(expected = NullPointerException.class)
    @Parameters(method = "getNullArray")
    public void testConstructorNull(Integer[] array) {
        Rotation<Integer> rotation = new Rotation<>(array);
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters(method = "getTooSmallIntegerArray")
    public void testConstructorIllegal(Integer[] array) {
        Rotation<Integer> rotation = new Rotation<>(array);
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters(method = "getIllegalRotations")
    public void textIllegalLeftRotation(Integer[] array, int rotations, String expected) {
        testRotateLeft(array, rotations, expected);
    }

    @Test
    @Parameters(method = "getCustomLeftRotationIntegerArray")
    public void testRotateLeft(Integer[] array, int rotations, String expected) {
        Rotation<Integer> rotation = new Rotation<>(array);

        if (rotations > 0) {
            System.out.println("Before " + rotations + " left rotations:");
            System.out.println(Arrays.toString(array));
        }

        rotation.left(rotations);

        assertArrayEquals(array, rotation.getArray());
        assertEquals(expected, rotation.toString(true));
        System.out.println("After " + rotations + " left rotations:");
        System.out.println(Arrays.toString(array));
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters(method = "getIllegalRotations")
    public void textIllegalRightRotation(Integer[] array, int rotations, String expected) {
        testRotateRight(array, rotations, expected);
    }

    @Test
    @Parameters(method = "getCustomRightRotationIntegerArray")
    public void testRotateRight(Integer[] array, int rotations, String expected) {
        Rotation<Integer> rotation = new Rotation<>(array);

        System.out.println("Before " + rotations + " right rotations:");
        System.out.println(Arrays.toString(array));

        rotation.right(rotations);

        assertArrayEquals(array, rotation.getArray());
        assertEquals(expected, rotation.toString(true));
        System.out.println("After " + rotations + " right rotations:");
        System.out.println(Arrays.toString(array));
    }

    @Test
    @Parameters(method = "getCustomRightRotationIntegerArray")
    public void testObjectMethods(Integer[] array, int rotations, String expected) {
        Rotation<Integer> rotation = new Rotation<>(array);
        Rotation<Integer> rotation2 = new Rotation<>(array);

        rotation.right(rotations);
        rotation2.right(rotations);

        assertEquals(rotation.hashCode(), rotation2.hashCode());
        assertEquals(rotation, rotation2);
        assertEquals(rotation.toString(false), rotation2.toString(false));
    }
}