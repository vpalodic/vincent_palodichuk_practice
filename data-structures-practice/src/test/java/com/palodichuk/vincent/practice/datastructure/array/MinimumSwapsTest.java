package com.palodichuk.vincent.practice.datastructure.array;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(JUnitParamsRunner.class)
public class MinimumSwapsTest {

    private Object[] getNullArray() {
        return new Object[] {
                new Object[] {null, 1}
        };
    }

    private Object[] getTooSmallArray() {
        return new Object[] {
                new Object[] { new int[] {1}, 1},
                new Object[] { new int[] {}, 1}
        };
    }

    private Object[] getCustomArrayData() {
        return new Object[] {
                new Object[] { new int[] {4, 3, 1, 2}, 3},
                new Object[] { new int[] {2, 3, 4, 1, 5}, 3},
                new Object[] { new int[] {1, 3, 5, 2, 4, 6, 8}, 3}

        };
    }

    @Test(expected = NullPointerException.class)
    @Parameters(method = "getNullArray")
    public void testConstructorNullArray(int[] array, int expected) {
        testSort(array, expected);
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters(method = "getTooSmallArray")
    public void testConstructorTooSmallArray(int[] array, int expected) {
        testSort(array, expected);
    }

    @Test
    @Parameters(method = "getCustomArrayData")
    public void testSort(int[] array, int expected) {
        MinimumSwaps minimumSwaps = new MinimumSwaps(array);
        System.out.println("Before sorting:");
        System.out.println(Arrays.toString(array));

        assertEquals(expected, minimumSwaps.sort());

        System.out.println("After sorting:");
        System.out.println(Arrays.toString(array));
        System.out.println("Minimum swaps: " + expected);
    }
}