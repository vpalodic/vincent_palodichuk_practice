# PRACTICE #

How to build and run Practice

### What is this repository for? ###

* This is a collection of practice exercises
* Current version is 1.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

If you don't have the source you first need to clone the repository.

* If you are unable to clone the repository you may not have permission.
If that is the case, send me an e-mail with your bitbucket id and I will add
you to the repository.

### Building Practice ###

On Windows:

* Open a command window and navigate to the root of the project folder
* .\gradlew build

On *nix based systems:

* Open a shell window and navigate to the root of the project folder
* ./gradle build

The above will build Practice using Gradle and execute
any tests in the project.

### Running Practice After Building With Gradle ###

On Windows:

* Open a command window and navigate to the root of the project folder
* Ensure that you have already built the project using .\gradlew build
* .\gradlew runJar

On *nix based systems:

* Open a shell window and navigate to the root of the project folder
* Ensure that you have already built the project using ./gradle build
* ./gradle runJar

The above will run Practice and output the results to the console

### Building the JavaDoc ###

On Windows:

* Open a command window and navigate to the root of the project folder
* .\gradlew javadoc

On *nix based systems:

* Open a shell window and navigate to the root of the project folder
* ./gradle javadoc

The above command will build the JavaDoc for the project. The JavaDoc can then be
viewed in a web browser by opening ./build/docs/index.html

