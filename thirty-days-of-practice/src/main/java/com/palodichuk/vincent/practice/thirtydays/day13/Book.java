/*
 * File: Book.java
 */
package com.palodichuk.vincent.practice.thirtydays.day13;

/**
 * The Book class from Day 13 of Hacker Rank's 30 Days of Code.
 *
 */
public abstract class Book {
    /**
     * The title of this book.
     */
    protected String title;

    /**
     * The author of this book.
     */
    protected String author;

    /**
     * Instantiates a new Book with the specified title and author.
     *
     * @param title The book's title.
     * @param author The book's author.
     **/
    public Book(String title, String author) {
        this.title = title;
        this.author = author;
    }

    /**
     * Returns a formatted string.
     *
     * @return a formatted string.
     */
    public abstract String display();
}
