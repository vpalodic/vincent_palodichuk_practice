/*
 * File: MyBook.java
 */
package com.palodichuk.vincent.practice.thirtydays.day13;

/**
 * The MyBook class from Day 13 of Hacker Rank's 30 Days of Code.
 *
 */
public class MyBook extends Book {
    private int price;

    /**
     * Instantiates a new MyBook with the specified title, author, and price
     *
     * @param title The book's title.
     * @param author The book's author.
     * @param price The book's price.
     **/
    public MyBook(String title, String author, int price) {
        super(title, author);
        this.price = price;
    }

    /**
     * Returns the following formatted string:<br><br>
     *
     * Title: title<br>
     * Author: author<br>
     * Price: price<br>
     *
     * @return a formatted string containing title, author, and price.
     */
    @Override
    public String display() {
        StringBuilder sb = new StringBuilder();

        sb.append("Title: ");
        sb.append(title);
        sb.append("\n");
        sb.append("Author: ");
        sb.append(author);
        sb.append("\n");
        sb.append("Price: ");
        sb.append(price);
        sb.append("\n");

        return sb.toString();
    }
}
