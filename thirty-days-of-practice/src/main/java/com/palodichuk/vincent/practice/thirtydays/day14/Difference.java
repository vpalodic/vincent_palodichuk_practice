/*
 * File: Difference.java
 */
package com.palodichuk.vincent.practice.thirtydays.day14;

import java.util.Arrays;

/**
 * The absolute difference between two integers, a and b , is written as | a - b |.
 * The maximum absolute difference between two integers in a set of positive integers,
 * elements, is the largest absolute difference between any two integers in elements.
 */
public class Difference {
    private int[] elements;

    /**
     * The maximum absolute difference between two elements.
     */
    public int maximumDifference;

    /**
     * Instantiates this instance with the specified array of integers.
     *
     * @param elements the array of integers to compute the maximum absolute difference
     */
    public Difference(int[] elements) {
        this.elements = Arrays.copyOf(elements, elements.length);
    }

    /**
     * Computes the maximum absolute difference between any 2 numbers in
     * the stored array and stores it in the maximumDifference instance variable.
     */
    public void computeDifference() {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < elements.length; i++) {
            if (elements[i] < min)
                min = elements[i];
            if (elements[i] > max)
                max = elements[i];
        }

        maximumDifference = Math.abs(max - min);
    }
}
