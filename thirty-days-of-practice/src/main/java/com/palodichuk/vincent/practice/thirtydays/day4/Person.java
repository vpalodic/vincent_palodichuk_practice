/*
 * File: Person.java
 */
package com.palodichuk.vincent.practice.thirtydays.day4;

/**
 * A simple person class that keeps track of an age and tells you if you are old.
 */
public class Person {
    private int age;

    /**
     * Instantiates a new Person with the specified age
     * @param initialAge the initial age of this person     *
     */
    public Person(int initialAge) {
        if (initialAge < 0) {
            System.out.println("Age is not valid, setting age to 0.");
            age = 0;
        } else {
            age = initialAge;
        }
    }

    /**
     * Says you are tool old if 18 or over. you are young for under 13 and teenager for the rest.
     *
     * @return a string that states if you are old, a teenager, or young.
     */
    public String amIOld() {
        String msg;

        if (age < 13) {
            msg = "You are young.";
        } else if (age >= 13 && age < 18) {
            msg = "You are a teenager.";
        } else {
            msg = "You are old.";
        }

        return msg;
    }

    /**
     * Increases this person's age by one year.
     */
    public void yearPasses() {
        age++;
    }

    /**
     * Gets age
     *
     * @return value of age
     */
    public int getAge() {
        return age;
    }
}