package com.palodichuk.vincent.practice.thirtydays.day14;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(JUnitParamsRunner.class)
public class DifferenceTest {

    private Object[] getArrayData() {
        return new Object[] {
                new Object[] { new int[] {1, 2, 5}, 4},
                new Object[] { new int[] {1, 2, 3, 4, 5, 6}, 5},
                new Object[] { new int[] {2, 2, 2, 2, 2, 2, 2}, 0},
                new Object[] { new int[] {2, 1, 2, 1, 2, 1, 2, 1}, 1}
        };
    }

    @Test
    @Parameters(method = "getArrayData")
    public void computeDifference(int[] array, int expected) {
        Difference difference = new Difference(array);

        difference.computeDifference();

        assertEquals(expected, difference.maximumDifference);
    }
}