package com.palodichuk.vincent.practice.thirtydays.day4;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(JUnitParamsRunner.class)
public class PersonTest {

    private static Object[] getCustomAmIOldData() {
        return new Object[] {
                new Object[] {-1, "You are young."},
                new Object[] {0, "You are young."},
                new Object[] {12, "You are young."},
                new Object[] {13, "You are a teenager."},
                new Object[] {17, "You are a teenager."},
                new Object[] {18, "You are old."},
                new Object[] {99, "You are old."}
        };
    }

    private static Object[] getCustomYearPasses() {
        return new Object[] {
                new Object[] {-1, 3, 3},
                new Object[] {0, 3, 3},
                new Object[] {12, 5, 17},
                new Object[] {13, 1, 14},
                new Object[] {17, 10, 27},
                new Object[] {18, 6, 24},
                new Object[] {99, 6, 105}
        };
    }

    @Test
    @Parameters(method = "getCustomAmIOldData")
    public void amIOld(int age, String expected) {
        Person person = new Person(age);

        assertEquals(expected, person.amIOld());
    }

    @Test
    @Parameters(method = "getCustomYearPasses")
    public void yearPasses(int age, int yearsToPass, int expectedNewAge) {
        Person person = new Person(age);

        for (int i = 0; i < yearsToPass; i++)
            person.yearPasses();

        assertEquals(expectedNewAge, person.getAge());
    }
}