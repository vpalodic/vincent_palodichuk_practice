package com.palodichuk.vincent.practice.thirtydays.day13;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(JUnitParamsRunner.class)
public class MyBookTest {

    private Object[] getCustomBookData() {
        return new Object[] {
                new Object[] {"The Alchemist", "Paulo Coelho", 248,
                        "Title: The Alchemist\nAuthor: Paulo Coelho\nPrice: 248\n"},
                new Object[] {"The Greenway", "Nick Palodichuk", 1497,
                        "Title: The Greenway\nAuthor: Nick Palodichuk\nPrice: 1497\n"},
        };
    }

    @Test
    @Parameters(method = "getCustomBookData")
    public void testDisplay(String title, String author, int price, String expected) {
        Book book = new MyBook(title, author, price);

        assertEquals(expected, book.display());
    }
}