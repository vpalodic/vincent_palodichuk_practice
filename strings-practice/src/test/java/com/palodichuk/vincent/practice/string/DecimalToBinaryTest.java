package com.palodichuk.vincent.practice.string;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class DecimalToBinaryTest {
    private static Object[] getCustomValidTestNumbers() {
        return new Object[] {
                new Object[] {-9_223_372_036_854_775_808L, "1000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000", 1L},
                new Object[] {-6974716303342280057L, "1001 1111 0011 0100 1101 0100 0111 1001 1100 1010 0010 1000 0110 0010 1000 0111", 5L},
                new Object[] {-512L, "1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1110 0000 0000", 55L},
                new Object[] {-3L, "1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1101", 62L},
                new Object[] {-2L, "1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1110", 63L},
                new Object[] {-1L, "1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111", 64L},
                new Object[] {1L, "0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0001", 1L},
                new Object[] {2L, "0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0010", 1L},
                new Object[] {3L, "0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0011", 2L},
                new Object[] {512L, "0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0010 0000 0000", 1L},
                new Object[] {9_223_372_036_854_775_807L, "0111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111", 63L},
        };
    }

    @Test
    @Parameters(method = "getCustomValidTestNumbers")
    public void testCalculate(long num, String expectedStr, long expectedMax) {
        final long expectedNum = num;

        DecimalToBinary decimalToBinary = new DecimalToBinary(num);

        assertEquals(expectedNum, decimalToBinary.getNumber());
        decimalToBinary.calculate();

        System.out.println("Decimal: " + num);

        String bitString = decimalToBinary.getBitString();

        assertEquals(expectedStr, bitString);
        System.out.println("Bit string: " + bitString);

        long maxOnesInARow = decimalToBinary.getMaxOnesInARow();

        assertEquals(expectedMax, maxOnesInARow);
        System.out.println("Maximum number of ones in a row: " + decimalToBinary.getMaxOnesInARow());
    }
}