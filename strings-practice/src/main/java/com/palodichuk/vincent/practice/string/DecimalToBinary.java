/*
 * File: DecimalToBinary.java
 */
package com.palodichuk.vincent.practice.string;

import java.util.Stack;

/**
 * A class to convert a 64-bit signed decimal number to
 * a binary string. Also counts the maximum number of ones
 * in a row found in the binary string.
 */
public class DecimalToBinary {
    /**
     * The bits in the string are grouped by nibbles.
     * This constant details how many bits are in a nibble.
     */
    public static final int BITS_IN_NIBBLE = 4;

    /**
     * A 64-bit number contains 8 bytes which can be broken
     * down in to 16 nibbles.
     */
    public static final int NUM_NIBBLES = 16;

    private final long number;
    private long maxOnesInARow;
    private String bitString;

    /**
     * Instantiates a new instance with the specified 64-bit
     * signed integer.
     *
     * @param number the 64-bit signed integer to convert to binary.
     */
    public DecimalToBinary(long number) {
        this.number = number;
        this.maxOnesInARow = -1;
        this.bitString = "";
    }

    /**
     * Returns the base 10 number.
     *
     * @return the 64-bit signed base 10 number.
     */
    public long getNumber() {
        return number;
    }

    /**
     * Returns the maximum number of ones in a row in the bit string or Integer.MIN_VALUE
     * if calculate has not been called.
     *
     * @return the maximum number of ones in a row in the bit string.
     */
    public long getMaxOnesInARow() {
        return maxOnesInARow;
    }

    /**
     * Returns the binary bit string or the empty string if calculate hasn't been called.
     *
     * @return the binary representation of the signed 64-bit base 10 number.
     */
    public String getBitString() {
        return bitString;
    }

    private static long maxOnesInARow(Stack<Integer> bits, StringBuilder sb) {
        long maxInARow = 0;
        long currentRun = 0;
        long numNibbles = bits.size() / BITS_IN_NIBBLE;
        int bitsRemaining = bits.size() % BITS_IN_NIBBLE == 0 ? BITS_IN_NIBBLE : bits.size() % BITS_IN_NIBBLE;

        // Check for partial group
        if (bitsRemaining < BITS_IN_NIBBLE && !bits.isEmpty()) {
            int count = 0;

            while (count < BITS_IN_NIBBLE - bitsRemaining) {
                bits.push(0);
                count++;
            }

            if (count > 0)
                numNibbles++;
        }

        // Ensure we have
        while (numNibbles < NUM_NIBBLES) {
            for (int i = 0; i < BITS_IN_NIBBLE; i++) {
                bits.push(0);
            }
            numNibbles++;
        }

        bitsRemaining = BITS_IN_NIBBLE;

        while (!bits.isEmpty()) {
            int bit = bits.pop();
            sb.append(bit);

            if (bit == 0) {
                if (currentRun > maxInARow)
                    maxInARow = currentRun;
                currentRun = 0;
            } else {
                currentRun++;
            }

            if (--bitsRemaining <= 0 && !bits.isEmpty()) {
                sb.append(" ");
                bitsRemaining = BITS_IN_NIBBLE;
            }
        }

        if (currentRun > maxInARow)
            maxInARow = currentRun;

        return maxInARow;
    }

    private static String twosCompliment(String bin) {
        String twos = "", ones = "";

        for (int i = 0; i < bin.length(); i++) {
            ones += flip(bin.charAt(i));
        }

        StringBuilder builder = new StringBuilder(ones);
        boolean b = false;
        int numSpaces = 0;
        for (int i = ones.length() - 1; i > 0; i--) {
            if (ones.charAt(i) == '1') {
                builder.setCharAt(i, '0');
            } else if (ones.charAt(i) == '0') {
                builder.setCharAt(i, '1');
                b = true;
                break;
            } else {
                numSpaces++;
                continue;
            }
        }

        if (!b)
            builder.setCharAt(0, '1');

        StringBuilder nums = new StringBuilder();

        twos = nums.toString() + builder.toString();

        return twos;
    }

    // Returns '0' for '1' and '1' for '0'
    private static char flip(char c) {
        return (c == '0') ? '1' : (c == '1') ? '0' : c;
    }

    /**
     * Calculates the bit string and max number of ones in a row.
     */
    public void calculate() {
        long result = number;
        long remainder = 0;
        Stack<Integer> bits = new Stack<>();

        do {
            remainder = Math.abs(result % 2);
            result = result / 2;

            if (remainder > 0) {
                bits.push(1);
            } else {
                bits.push(0);
            }
        } while (result != 0);

        StringBuilder sb = new StringBuilder();

        long maxInARow = maxOnesInARow(bits, sb);

        bitString = sb.toString();

        if (number < 0) {
            bitString = twosCompliment(bitString);

            for (int i = bitString.length() - 1; i >= 0; i--) {
                try {
                    bits.push(Integer.parseInt(String.valueOf(bitString.charAt(i))));
                } catch (NumberFormatException ex) {

                }
            }

            maxInARow = maxOnesInARow(bits, sb);
        }

        maxOnesInARow = maxInARow;
    }
}
