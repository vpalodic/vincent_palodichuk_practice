package com.palodichuk.vincent.practice.problemsolving;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(JUnitParamsRunner.class)
public class DivisibleSumPairsTest {

    private Object[] getNullArray() {
        return new Object[] {
                new Object[] { 2, null, 0 }
        };
    }

    private static Object[] getTooSmallIntegerArray() {
        return new Object[] {
                new Object[] { 2, new int[] {1}, 0 },
                new Object[] { 1, new int[] {}, 0 }
        };
    }

    private Object[] getCustomArrayData() {
        return new Object[] {
                new Object[] {3, new int[] {1, 3, 2, 6, 1, 2}, 5},
                new Object[] {5, new int[] {1, 2, 3, 4, 5, 6}, 3},
                new Object[] {4, new int[] {1, 3, 2, 6, 1, 2}, 5},
                new Object[] {2, new int[] {1, 3, 2, 6, 1, 2}, 6},
                new Object[] {6, new int[] {1, 3, 2, 6, 1, 2}, 0},
                new Object[] {7, new int[] {1, 3, 2, 6, 1, 2}, 2}
        };
    }

    @Test(expected = NullPointerException.class)
    @Parameters(method = "getNullArray")
    public void testNullConstructor(int k, int[] arr, int expected) {
        testCalculate(k, arr, expected);
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters(method = "getTooSmallIntegerArray")
    public void testTooSmallConstructor(int k, int[] arr, int expected) {
        testCalculate(k, arr, expected);
    }

    @Test
    @Parameters(method = "getCustomArrayData")
    public void testCalculate(int k, int[] arr, int expected) {
        DivisibleSumPairs divisibleSumPairs = new DivisibleSumPairs(arr);

        assertEquals(expected, divisibleSumPairs.calculate(k));
    }
}