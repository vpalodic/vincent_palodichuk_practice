/*
 * File: DivisibleSumPairs.java
 */
package com.palodichuk.vincent.practice.problemsolving;

/**
 * You are given an array of n integers, ar = [ar[0], ar[1], ..., ar[n - 1],
 * and a positive integer, k. Find and print the number of (i, j) pairs
 * where i &lt; j and ar[i] + ar[j] is divisible by k.
 *
 * For example, ar = [1, 2, 3, 4, 5, 6] and k = 5. Our three pairs meeting
 * the criteria are [1, 4], [2, 3], and [4, 6].
 */
public class DivisibleSumPairs {
    private int[] array;

    /**
     * Constructs a new instance based on the specified array.
     *
     * @param array the integer array to find the i and j paris from that are divisible by k.
     * @throws NullPointerException indicates that the specified array is null
     * @throws IllegalArgumentException indicates that the specified array contains less than 2 elements.
     */
    public DivisibleSumPairs(int[] array) {
        if (array == null) {
            throw new NullPointerException("The specified array is null.");
        }

        if (array.length < 2) {
            throw new IllegalArgumentException("The specified array contains fewer than 2 elements.");
        }

        this.array = array;
    }

    /**
     * Calculates the number of [i, j] pairs whose sum is evenly divisible by the specified k.
     *
     * @param k the number to divide the sums by.
     * @return the number of [i, j] pairs whose sum is evenly divisible by the specified k.
     */
    public int calculate(int k) {
        int answer = 0;
        int n = array.length;

        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if ((array[i] + array[j]) % k == 0) {
                    answer++;
                }
            }
        }

        return answer;
    }
}
